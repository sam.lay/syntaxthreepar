# (c) Copyright 2019-2020 Syntax Systems
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
"""
HPE 3PAR Client.

:Author: Samuel Lay
:Copyright: Copyright Copyright 2019-2020 Syntax Systems
:License: Apache v2.0

"""
# from typing import Any, Union

from hpe3parclient import client
from hpe3parclient import exceptions
# from hpe3parclient import ssh
# from pprint import pprint
import enum
import socket
from csv import reader

dnsPath = tuple('.dc.fit .us.fit'.split())


class ArrayType(enum.Enum):
    netapp = 1
    emc = 2
    _3par = 3


class StorageObject:

    def __init__(self, name=None):
        self.name = name


class Volume(StorageObject):
    def __new__(cls, *args, **kwargs):
        instance = super(Volume, cls).__new__(cls, *args, **kwargs)
        return instance

    def __init__(self, vol_type=None, name=None, vol_object=None):

        super().__init__(name)
        self.name = name
        self.vol_type = vol_type
        self.vol_object = vol_object


class Array(StorageObject):

    arrays = []

    def __new__(cls, *args, **kwargs):
        instance = super(Array, cls).__new__(cls)
        return instance

    def __init__(self, name, array_type=None, ip=None, timeout=None):
        super().__init__(name)
        self.name = name
        self.client = None
        self.array_type = array_type
        self.timeout = timeout
        self.ip = ip

        # if self.name is not None:
        #     self.read_params()
        # else:
        #     self.hpe_params = {}

        self.volumes = None
        self.volumeSets = None
        self.vluns = None
        self.hosts = None
        self.hostSets = None
        self.remoteCopyGroups = None
        self.schedules = None
        self.schedule_strings = None
        self.ports = None
        self.storageSystemInfo = None
        if self.ip is None:
            try:
                self.getIp()
            except:                   # Explicitly cite appropriate exceptions by name
                pass
        Array.arrays.append(self)

    def hasVluns(self):
        return bool(self.vluns)

    def hasVolumes(self):
        return bool(self.volumes)

    def hasIp(self):
        return bool(self.ip)

    def hasSchedules(self):
        return bool(self.schedules)

    def hasHosts(self):
        return bool(self.hosts)

    def getIp(self):
        if self.name is None:
            raise NameError('The array has no name, cannot resolve IP address')
        try:
            self.ip = socket.gethostbyname(self.name)
        except socket.gaierror:
            for suffix in dnsPath:
                try:
                    self.ip = socket.gethostbyname(self.name + suffix)
                except socket.gaierror:
                    pass
        if self.ip is None:
            raise
        return self.ip

    def connect(self, username, password):
        url = 'https://' + self.ip + ':8080/api/v1'
        self.client = client.HPE3ParClient(url, suppress_ssl_warnings=True)
        self.client.setSSHOptions(self.ip, username, password)
        try:
            self.client.login(username, password)
        except exceptions.HTTPUnauthorized as ex:
            print("Login failed.")
            raise ex
        self.getStorageSystemInfo()

    def disconnect(self):
        try:
            self.client.logout()
        except:                          # Need to explicitly identify exceptions
            pass

    def getStorageSystemInfo(self):
        self.storageSystemInfo = self.client.getStorageSystemInfo()

    def getVolumes(self):
        self.volumes = self.client.getVolumes()['members']

    def getVluns(self):
        self.vluns = self.client.getVLUNs()['members']

    def getVolumeList(self):
        if self.volumes is None:
            self.getVolumes()
        vol_list = []
        for vol in self.volumes:
            vol_list.append(vol['name'])
        return vol_list

    def getBaseVolumeList(self):
        if self.volumes is None:
            self.getVolumes()
        vol_list = []
        for vol in self.volumes:
            if vol['id'] == vol['baseId']:
                vol_list.append(vol['name'])
        return vol_list

    def getVolumeSets(self):
        self.volumeSets = self.client.getVolumeSets()['members']

    def getHosts(self):
        self.hosts = self.client.getHosts()['members']

    def getHostSets(self):
        self.hostSets = self.client.getHostSets()['members']
        
    def getSchedules(self):
        self.schedules = []
        schedule_strings = self.client.getSchedule("*")
        headers = 'SchedName,Command,Min,Hour,DOM,Month,DOW,CreatedBy,Status,Alert,NextRunTime'
        for schedule_record in reader(schedule_strings[2:-3]):
            sched = dict(zip(headers.split(','), schedule_record))
            self.schedules.append(sched)

    def createSchedule(self, schedule_name, task, taskfreq):
        self.client.createSchedule(schedule_name, task, taskfreq)

    def getRemoteCopyGroups(self):
        try:
            self.remoteCopyGroups = self.client.getRemoteCopyGroups()['members']
            # Not all arrays have remote copy licenses.
        except:                                           # Need to explicity handle named exceptions
            pass

    def getPorts(self):
        self.ports = self.client.getPorts()['members']

    def getFCPorts(self):
        self.ports = self.client.getFCPorts()
