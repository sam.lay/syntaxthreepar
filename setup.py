import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="three_par", # Replace with your own username
    version="0.0.1",
    author="Samuel Lay",
    author_email="sam.lay@syntax.com",
    description="This three_par library defines objects and methods for accessing HPE 3-Par storage arrays",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/sam.lay/three_par",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Apache Software License"
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)