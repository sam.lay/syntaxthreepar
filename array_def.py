# (c) Copyright 2019-2020 Syntax Systems
# All Rights Reserved.
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.
"""
HPE 3PAR Client.

:Author: Samuel Lay
:Copyright: Copyright Copyright 2019-2020 Syntax Systems
:License: Apache v2.0

"""

dc8_3pars = tuple('ask01a08pp001  3par801        3par802        fit00f08pp0813 \
                   fit00a08pp803  fit01c08pp003  fit00z08pp805  fit00a08pp804  \
                   fit00b08pp806  ngr01e08pp1806 ngr01e08pp1807 glz00d08pp807  \
                   neo01d08pp005  fit00c08pp808  fit00e08pp0809 fit00z08pp0810 \
                   fit00y08pp0811 fit00z08pp0812 fit02a08pp2801 lab08pp001'.split())

dc9_3pars = tuple('ask01a09pp002  3par901        3par902        fit00f08pp0813 \
                   fit00a09pp903  fit01c09pp004  fit00z09pp905  fit00a09pp904  \
                   fit00b09pp906  ngr01e09pp1906 ngr01e09pp1907 glz00d09pp907  \
                   neo01d09pp005  fit00c09pp908  fit00e09pp0909 fit00z09pp0910 \
                   fit00y09pp0911 fit00z09pp0912 fit02a09pp2901 lab09pp002'.split())

# all_3pars = dc8_3pars + dc9_3pars
all_3pars = []
mirror = {}

for pair in zip(dc8_3pars, dc9_3pars):
    for indiv in pair:
        all_3pars.append(indiv)

all_3pars.append('fit98a10pp001')

fqdn3par = {'ask01a08pp001':  'ask01a08pp001.us.fit',
            'ask01a09pp002':  'ask01a09pp002.us.fit',
            '3par801':        '3par801.dc.fit',
            '3par901':        '3par901.dc.fit',
            '3par802':        '3par802.dc.fit',
            '3par902':        '3par902.dc.fit',
            'fit00a08pp803':  'fit00a08pp803.dc.fit',
            'fit00a09pp903':  'fit00a09pp903.dc.fit',
            'fit01c08pp003':  'fit01c08pp003.us.fit',
            'fit01c09pp004':  'fit01c09pp004.us.fit',
            'fit00a08pp804':  'fit00a08pp804.dc.fit',
            'fit00a09pp904':  'fit00a09pp904.dc.fit',
            'fit00z08pp805':  'fit00z08pp805.us.fit',
            'fit00z09pp905':  'fit00z09pp905.us.fit',
            'fit00b08pp806':  'fit00b08pp806.us.fit',
            'fit00b09pp906':  'fit00b09pp906.us.fit',
            'glz00d08pp807':  'glz00d08pp807.us.fit',
            'glz00d09pp907':  'glz00d09pp907.us.fit',
            'neo01d08pp005':  'neo01d08pp005.us.fit',
            'neo01d09pp005':  'neo01d09pp005.us.fit',
            'fit00c08pp808':  'fit00c08pp808.us.fit',
            'fit00c09pp908':  'fit00c09pp908.us.fit',
            'fit00e08pp0809': 'fit00e08pp0809.us.fit',
            'fit00e09pp0909': 'fit00e09pp0909.us.fit',
            'fit00z08pp0810': 'fit00z08pp0810.dc.fit',
            'fit00z09pp0910': 'fit00z09pp0910.dc.fit',
            'fit00y08pp0811': 'fit00y08pp0811.dc.fit',
            'fit00y09pp0911': 'fit00y09pp0911.dc.fit',
            'fit00z08pp0812': 'fit00z08pp0812.dc.fit',
            'fit00z09pp0912': 'fit00z09pp0912.dc.fit',
            'fit00f08pp0813': 'fit00f08pp0813.us.fit',
            'fit00f09pp0913': 'fit00f09pp0913.us.fit',
            'ngr01e08pp1806': 'ngr01e08pp1806.us.fit',
            'ngr01e09pp1906': 'ngr01e09pp1906.us.fit',
            'ngr01e08pp1807': 'ngr01e08pp1807.us.fit',
            'ngr01e09pp1907': 'ngr01e09pp1907.us.fit',
            'fit02a08pp2801': 'fit02a08pp2801.us.fit',
            'fit02a09pp2901': 'fit02a09pp2901.us.fit',
            'fit98a10pp001':  'fit98a10pp001.us.fit',
            'lab08pp001':     'lab08pp001.us.fit',
            'lab09pp002':     'lab09pp002.us.fit'}

short_names = {'ask01a08pp001':  'p0101',
               'ask01a09pp002':  'p0102',
               '3par801':        'p0801',
               '3par901':        'p0901',
               '3par802':        'p0802',
               '3par902':        'p0902',
               'fit00a08pp803':  'p0803',
               'fit00a09pp903':  'p0903',
               'fit01c08pp003':  'p1803',
               'fit01c09pp004':  'p1904',
               'fit00a08pp804':  'p0804',
               'fit00a09pp904':  'p0904',
               'fit00z08pp805':  'p0805',
               'fit00z09pp905':  'p0905',
               'fit00b08pp806':  'p0806',
               'fit00b09pp906':  'p0906',
               'glz00d08pp807':  'p0807',
               'glz00d09pp907':  'p0907',
               'neo01d08pp005':  'p1805',
               'neo01d09pp005':  'p1905',
               'fit00c08pp808':  'p0808',
               'fit00c09pp908':  'p0908',
               'fit00e08pp0809': 'p0809',
               'fit00e09pp0909': 'p0909',
               'fit00z08pp0810': 'p0810',
               'fit00z09pp0910': 'p0910',
               'fit00y08pp0811': 'p0811',
               'fit00y09pp0911': 'p0911',
               'fit00z08pp0812': 'p0812',
               'fit00z09pp0912': 'p0912',
               'fit00f08pp0813': 'p0813',
               'fit00f09pp0913': 'p0913',
               'ngr01e08pp1806': 'p1806',
               'ngr01e09pp1906': 'p1906',
               'ngr01e08pp1807': 'p1807',
               'ngr01e09pp1907': 'p1907',
               'fit02a08pp2801': 'p2801',
               'fit02a09pp2901': 'p2901',
               'fit98a10pp001':  'p9801',
               'lab08pp001':     'plab81',
               'lab09pp002':     'plab92'}

mirror = dict(zip(dc9_3pars, dc8_3pars))
mirror.update(dict(zip(dc8_3pars, dc9_3pars)))

raidsize = {'3par801':        4,
            '3par901':        4,
            '3par802':        5,
            '3par902':        5,
            'fit00a08pp803':  4,
            'fit00a09pp903':  4,
            'fit00a08pp804':  4,
            'fit00a09pp904':  4,
            'fit00z08pp805':  4,
            'fit00z09pp905':  4,
            'fit00b08pp806':  4,
            'fit00b09pp906':  4,
            'glz00d08pp807':  6,
            'glz00d09pp907':  6,
            'fit00c08pp808':  4,
            'fit00c09pp908':  4,
            'fit00e08pp0809': 4,
            'fit00e09pp0909': 4,
            'fit00z08pp0810': 4,
            'fit00z09pp0910': 4,
            'fit00y08pp0811': 4,
            'fit00y09pp0911': 4,
            'fit00z08pp0812': 4,
            'fit00z09pp0912': 4,
            'fit00f08pp0813': 4,
            'fit00f09pp0913': 4,
            'ask01a08pp001':  4,
            'ask01a09pp002':  4,
            'fit01c08pp003':  4,
            'fit01c09pp004':  4,
            'neo01d08pp005':  4,
            'neo01d09pp005':  4,
            'ngr01e08pp1806': 4,
            'ngr01e09pp1906': 4,
            'ngr01e08pp1807': 4,
            'ngr01e09pp1907': 4,
            'fit02a08pp2801': 4,
            'fit02a09pp2901': 4,
            'fit98a10pp001':  3,
            'lab08pp001':     4,
            'lab09pp002':     4}


filers = tuple('na013         na014         na015         na016\
                na019         na020         na021         na022\
                fitn09p023    fitn09p024    fitn09p025    fitn09p026\
                fitn09p027    fitn09p028    fitn09p029    fitn09p030\
                fitn08p031    fitn08p032    fitn09p033    fitn09p034\
                fit01b08np035 fit01b08np036 fit01b09np037 fit01b09np038'.split())

dc8filers = tuple('na013          na014\
                   na015          na016         na019       na020\
                   na021          na022         fitn08p031  fitn08p032\
                   fit01b08np035  fit01b08np036'.split())

dc9filers = tuple('fitn09p023 fitn09p024 fitn09p025    fitn09p026\
                   fitn09p027 fitn09p028 fitn09p029    fitn09p030\
                   fitn09p033 fitn09p034 fit01b09np037 fit01b09np038'.split())

haPairs = (('na013',         'na014'),         ('na015',         'na016'),
           ('na019',         'na020'),         ('na021',         'na022'),
           ('fitn09p023',    'fitn09p024'),    ('fitn09p025',    'fitn09p026'),
           ('fitn09p027',    'fitn09p028'),    ('fitn09p029',    'fitn09p030'),
           ('fitn08p031',    'fitn08p032'),    ('fitn09p033',    'fitn09p034'),
           ('fit01b08np035', 'fit01b08np036'), ('fit01b09np037', 'fit01b09np038'))

hyb3pars = tuple('3par801 3par901 3par802 3par902 fit00a08pp803 fit00a09pp903 \
                  fit00a08pp804 fit00a09pp904 fit00z08pp805 fit00z09pp905     \
                  fit00b08pp806 fit00b09pp906 fit00c08pp808 fit00c09pp908     \
                  ask01a08pp001 ask01a09pp002 fit01c08pp003 fit01c09pp004 '.split())
fc3pars = ()
ssd3pars = tuple('glz00d08pp807 glz00d09pp907 fit00e08pp0809 fit00e09pp0909 \
            fit00z08pp0810 fit00z09pp0910 fit00y08pp0811 fit00y09pp0911     \
            fit00z08pp0812 fit00z09pp0912 neo01d08pp005 neo01d09pp005       \
            ngr01e08pp1806 ngr01e09pp1906 ngr01e08pp1807 ngr01e09pp1907     \
            fit02a08pp2801 fit02a09pp2901 fit98a10pp001'.split())



descript = {'3par801':        'First Quality',
            '3par901':        'First Quality',
            '3par802':        'Energizer/Edgewell',
            '3par902':        'Energizer/Edgewell',
            'fit00a08pp803':  'Shared VDC00',
            'fit00a09pp903':  'Shared VDC00',
            'fit00a08pp804':  'Shared VDC00 Pod A (ESX Only)',
            'fit00a09pp904':  'Shared VDC00 Pod A (ESX Only)',
            'fit00z08pp805':  'Cintas VDC00',
            'fit00z09pp905':  'Cintas VDC00',
            'fit00b08pp806':  'Shared VDC00 Pod B (ESX Only)',
            'fit00b09pp906':  'Shared VDC00 Pod B (ESX Only)',
            'glz00d08pp807':  'Glazers VDC00 Pod D',
            'glz00d09pp907':  'Glazers VDC00 Pod D',
            'fit00c08pp808':  'Shared VDC00 Pod C (ESX Only)',
            'fit00c09pp908':  'Shared VDC00 Pod C (ESX Only)',
            'fit00e08pp0809': 'Shared VDC00 Pod E (ESX Only)',
            'fit00e09pp0909': 'Shared VDC00 Pod E (ESX Only)',
            'fit00z08pp0810': 'Cintas VDC00 (3par83 Replacement)',
            'fit00z09pp0910': 'Cintas VDC00 (3par94 Replacement)',
            'fit00y08pp0811': 'Shared VDC00 (VMAX Replacement)',
            'fit00y09pp0911': 'Shared VDC00 (VMAX Replacement)',
            'fit00z08pp0812': 'Cintas VDC00 (V-HANA)',
            'fit00z09pp0912': 'Cintas VDC00 (V-HANA)',
            'ask01a08pp001':  'ASK VDC01 Pod A',
            'ask01a09pp002':  'ASK VDC01 Pod A',
            'fit01c08pp003':  'Shared VDC01 Pod C (ESX Only)',
            'fit01c09pp004':  'Shared VDC01 Pod C (ESX Only)',
            'neo01d08pp005':  'Neovia VDC01 Pod D',
            'neo01d09pp005':  'Neovia VDC01 Pod D',
            'ngr01e08pp1806': 'National Grid VDC01 Pod E',
            'ngr01e09pp1906': 'National Grid VDC01 Pod E',
            'ngr01e08pp1807': 'National Grid VDC01 Pod E',
            'ngr01e09pp1907': 'National Grid VDC01 Pod E',
            'fit02a08pp2801': 'Shared VDC02',
            'fit02a09pp2901': 'Shared VDC02',
            'fit98a10pp001':  'Las Vegas DR Site',
            'lab08pp001':     'Snared Lab',
            'lab09pp002':     'Shared Lab'}





if (__name__ == '__main__'):
    print('It must have run . . .')